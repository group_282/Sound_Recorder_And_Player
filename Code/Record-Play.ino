#include <PWMAudio.h>

const int buttonPin = 28;
const int micPin = 26;
const int speakerPin = 1;
const int soundSize = 2000;

int16_t recording[soundSize];

PWMAudio pwm(speakerPin);

bool acting = false;

void setup() // put your setup code here, to run once:
{
  pinMode(buttonPin, INPUT);
  Serial.begin(9600);
  
}

void loop() // put your main code here, to run repeatedly:
{
  if (!acting && analogRead(buttonPin) > 500) record();
  

}

void record()
{
  acting = true;
  Serial.println("Record started");
  for(int a = 0; a < soundSize; ++a)
  {
    recording[a] = analogRead(micPin);
    delay(1);
  }
  Serial.println("Record ended");
  
  delay(500);

  //pwm.onTransmit(play);
  //pwm.begin(44100);
  
  Serial.println("Sound started");
  for(int a = 0; a < soundSize; ++a)
  {
    analogWrite(speakerPin, recording[a]);
    Serial.println(recording[a]);
    delay(1);
  }
  analogWrite(speakerPin, 0);
  Serial.println("Sound ended");

  acting = false;
  
}

void play()
{
  while (pwm.availableForWrite())
  {
    for (int a = 0; a < soundSize; ++a) 
    {
      pwm.write(recording[a]);
    }
    acting = false;
    pwm.end();
  }
}

